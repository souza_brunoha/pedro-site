$(document).ready(function(){
    $('.collapse').collapse();
    scrolledHeader();

    $(window).scroll(scrolledHeader);

    function scrolledHeader() {
        var top = $(document).scrollTop();

        if (top > 80) {
            $('header').addClass('scrolled');
            $('header img.header-white').show();
            $('header img.header-black').hide();

        } else {
            $('header').removeClass('scrolled');
            $('header img.header-white').hide();
            $('header img.header-black').show();
        }
    }

    $(".pedro-header-nav nav ul li a[href^='#']").smoothScroll({offset: -80});
    $(".pedro-header-sidenav nav ul li a[href^='#']").smoothScroll({
        offset: -80,
        beforeScroll: function() {
            $('.pedro-header-sidenav').toggleClass('show');
            $('.pedro-header-sidenavBg').toggleClass('show');
        }
    });

    $('.pedro-header-sidenavBtn').on('click', function(){
        $('.pedro-header-sidenav').toggleClass('show');
        $('.pedro-header-sidenavBg').toggleClass('show');
    });

    $('.pedro-header-sidenavBg').on('click', function() {
        $('.pedro-header-sidenav').toggleClass('show');
        $(this).toggleClass('show');                
    });
});